<link href="{{url('css/simple-sidebar.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{url('css/app.css')}}">
<!-- Sidebar -->
<div id="sidebar-wrapper" style="width:150px;margin-right:0;">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
        </li>
        <!-- <li>
            <a href="javascript:;" data-toggle="collapse" data-target="#categories"> Categories <i class="fa fa-fw fa-caret-down"></i></a>
            <ul id="categories" class="collapse">
                <li>
                    <a href="{{url('categories/create')}}">Add Category</a>
                </li>
                <li>
                    <a href="{{url('categories')}}">Manage Categories</a>
                </li>
            </ul>
        </li> -->
        <li>
            <a href="http://10.54.36.49/dashboard-license" style="font-size:16px">LICENSE</a>
        </li>
        <li>
            <a href="http://10.54.36.49/btsonair" style="font-size:16px">BTS ON AIR</a>
        </li>
        <li>
            <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" style="font-size:16px">NODIN</a>
        </li>
    </ul>
</div>
<!-- /#sidebar-wrapper -->

<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/bootstrap.bundle.min.js')}}"></script>
