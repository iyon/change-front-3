<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');

// Auth::routes();

// Route::get('/home', 'HomeController@index');

//dafinci 1-11
Route::get('dafinci', 'ApiController@dafinci');
Route::get('dafinci2', 'ApiController@dafinci2');
Route::get('dafinci3', 'ApiController@dafinci3');
Route::get('dafinci4', 'ApiController@dafinci4');
Route::get('dafinci5', 'ApiController@dafinci5');
Route::get('dafinci6', 'ApiController@dafinci6');
Route::get('dafinci7', 'ApiController@dafinci7');
Route::get('dafinci8', 'ApiController@dafinci8');
Route::get('dafinci9', 'ApiController@dafinci9');
Route::get('dafinci10', 'ApiController@dafinci10');
Route::get('dafinci11', 'ApiController@dafinci11');
//

//remedy 1-11
Route::get('remedy', 'ApiController@remedy');
Route::get('remedy2', 'ApiController@remedy2');
Route::get('remedy3', 'ApiController@remedy3');
Route::get('remedy4', 'ApiController@remedy4');
Route::get('remedy5', 'ApiController@remedy5');
Route::get('remedy6', 'ApiController@remedy6');
Route::get('remedy7', 'ApiController@remedy7');
Route::get('remedy8', 'ApiController@remedy8');
Route::get('remedy9', 'ApiController@remedy9');
Route::get('remedy10', 'ApiController@remedy10');
Route::get('remedy11', 'ApiController@remedy11');
//

//btsonair
Route::get('btsonair', 'ApiController@btsonair');
Route::get('btsonair2', 'ApiController@btsonair2');
Route::get('btsonair3', 'ApiController@btsonair3');
Route::get('btsonair4', 'ApiController@btsonair4');
Route::get('btsonair5', 'ApiController@btsonair5');
Route::get('btsonair6', 'ApiController@btsonair6');
Route::get('btsonair7', 'ApiController@btsonair7');
Route::get('btsonair8', 'ApiController@btsonair8');
Route::get('btsonair9', 'ApiController@btsonair9');
Route::get('btsonair10', 'ApiController@btsonair10');
Route::get('btsonair11', 'ApiController@btsonair11');
//

//nodin 1-11
Route::get('nodin', 'ApiController@nodin');
Route::get('nodin2', 'ApiController@nodin2');
Route::get('nodin3', 'ApiController@nodin3');
Route::get('nodin4', 'ApiController@nodin4');
Route::get('nodin5', 'ApiController@nodin5');
Route::get('nodin6', 'ApiController@nodin6');
Route::get('nodin7', 'ApiController@nodin7');
Route::get('nodin8', 'ApiController@nodin8');
Route::get('nodin9', 'ApiController@nodin9');
Route::get('nodin10', 'ApiController@nodin10');
Route::get('nodin11', 'ApiController@nodin11');
//

//license 1-11
Route::get('add_license', 'ApiController@add_license');
Route::get('add_license2', 'ApiController@add_license2');
Route::get('add_license3', 'ApiController@add_license3');
Route::get('add_license4', 'ApiController@add_license4');
Route::get('add_license5', 'ApiController@add_license5');
Route::get('add_license6', 'ApiController@add_license6');
Route::get('add_license7', 'ApiController@add_license7');
Route::get('add_license8', 'ApiController@add_license8');
Route::get('add_license9', 'ApiController@add_license9');
Route::get('add_license10', 'ApiController@add_license10');
Route::get('add_license11', 'ApiController@add_license11');
//

//active cell 1-11
Route::get('act_cell', 'ApiController@act_cell');
Route::get('act_cell2', 'ApiController@act_cell2');
Route::get('act_cell3', 'ApiController@act_cell3');
Route::get('act_cell4', 'ApiController@act_cell4');
Route::get('act_cell5', 'ApiController@act_cell5');
Route::get('act_cell6', 'ApiController@act_cell6');
Route::get('act_cell7', 'ApiController@act_cell7');
Route::get('act_cell8', 'ApiController@act_cell8');
Route::get('act_cell9', 'ApiController@act_cell9');
Route::get('act_cell10', 'ApiController@act_cell10');
Route::get('act_cell11', 'ApiController@act_cell11');
//

//deactive cell 1-11
Route::get('dea_cell', 'ApiController@dea_cell');
Route::get('dea_cell2', 'ApiController@dea_cell2');
Route::get('dea_cell3', 'ApiController@dea_cell3');
Route::get('dea_cell4', 'ApiController@dea_cell4');
Route::get('dea_cell5', 'ApiController@dea_cell5');
Route::get('dea_cell6', 'ApiController@dea_cell6');
Route::get('dea_cell7', 'ApiController@dea_cell7');
Route::get('dea_cell8', 'ApiController@dea_cell8');
Route::get('dea_cell9', 'ApiController@dea_cell9');
Route::get('dea_cell10', 'ApiController@dea_cell10');
Route::get('dea_cell11', 'ApiController@dea_cell11');
//

//active license 1-11
Route::get('act_license', 'ApiController@act_license');
Route::get('act_license2', 'ApiController@act_license2');
Route::get('act_license3', 'ApiController@act_license3');
Route::get('act_license4', 'ApiController@act_license4');
Route::get('act_license5', 'ApiController@act_license5');
Route::get('act_license6', 'ApiController@act_license6');
Route::get('act_license7', 'ApiController@act_license7');
Route::get('act_license8', 'ApiController@act_license8');
Route::get('act_license9', 'ApiController@act_license9');
Route::get('act_license10', 'ApiController@act_license10');
Route::get('act_license11', 'ApiController@act_license11');
//

//deactive license 1-11
Route::get('dea_license', 'ApiController@dea_license');
Route::get('dea_license2', 'ApiController@dea_license2');
Route::get('dea_license3', 'ApiController@dea_license3');
Route::get('dea_license4', 'ApiController@dea_license4');
Route::get('dea_license5', 'ApiController@dea_license5');
Route::get('dea_license6', 'ApiController@dea_license6');
Route::get('dea_license7', 'ApiController@dea_license7');
Route::get('dea_license8', 'ApiController@dea_license8');
Route::get('dea_license9', 'ApiController@dea_license9');
Route::get('dea_license10', 'ApiController@dea_license10');
Route::get('dea_license11', 'ApiController@dea_license11');
//

//change 1=11
Route::get('add_bts', 'ApiController@add_bts');
Route::get('add_bts2', 'ApiController@add_bts2');
Route::get('add_bts3', 'ApiController@add_bts3');
Route::get('add_bts4', 'ApiController@add_bts4');
Route::get('add_bts5', 'ApiController@add_bts5');
Route::get('add_bts6', 'ApiController@add_bts6');
Route::get('add_bts7', 'ApiController@add_bts7');
Route::get('add_bts8', 'ApiController@add_bts8');
Route::get('add_bts9', 'ApiController@add_bts9');
Route::get('add_bts10', 'ApiController@add_bts10');
Route::get('add_bts11', 'ApiController@add_bts11');
//

Route::get('chart_btsonair', 'ApiController@chart_btsonair');
Route::get('chart_nodin', 'ApiController@chart_nodin');
Route::get('chart_user', 'ApiController@chart_user');
Route::get('chart_dea', 'ApiController@chart_dea');

Route::get('cell_deactivated', 'ApiController@cell_deactivated');
Route::get('test', 'ApiController@test');

Route::get('index', 'ApiController@index');
Route::get('index2', 'ApiController@index2');
Route::get('index3', 'ApiController@index3');
Route::get('index4', 'ApiController@index4');
Route::get('index5', 'ApiController@index5');
Route::get('index6', 'ApiController@index6');
Route::get('index7', 'ApiController@index7');
Route::get('index8', 'ApiController@index8');
Route::get('index9', 'ApiController@index9');
Route::get('index10', 'ApiController@index10');
Route::get('index11', 'ApiController@index11');

Route::get('plan_remedy', 'ApiController@plan_remedy');
Route::get('plan_onair', 'ApiController@plan_onair');

